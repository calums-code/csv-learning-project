"""
On startup, read a .csv file containing various field regarding students.

Create a class with data members to store each student field.

Load each row into an instance of the class.

Create member functions to create, edit, view and delete instances, using user input.

Save modified data to .csv

"""
import csv


class Student ():
    """
    student class containing data members
    """
    #data objects
    #first name, last name,age, year, department, predictedGrade

    def __init__(self,first,last,age,year,department,predictedGrade):
      
        self.first = first
        self.last = last
        self.age = age
        self.year = year
        self.department = department
        self.predictedGrade = predictedGrade
    

    


    def printRow(self,):
        """
        Prints the data about a student
        """
        print ('Name: '+self.first + ' '+ self.last + ' age: ' + self.age + ' year: ' + self.year + ' department: ' + self.department + ' predictedGrade: '+ self.predictedGrade)
        pass
    

class csvReader ():
    """
    The csvReader class deals with opening, reading and writing to the the csv file
    """
    def __init__(self, filepath):

        self.filepath = filepath
        self.students = list()
        self.header = ''

    #functions: load, save

    #function that opens the csv file and copys the data members to the student class
    def load(self):
        with open(self.filepath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            

            for row in csv_reader:
                if line_count == 0:
                    self.header = row
                else:
                    self.students.append(Student(row[0],row[1],row[2],row[3],row[4],row[5])) #probably a much cleaner way to do this
                
                line_count += 1

    #function that saves each student member to the csv file
    def save(self):
        with open(self.filepath, mode='w', newline='') as csv_file:
            fieldnames = ['first', 'last', 'age', 'year', 'department', 'predictedGrade']
            csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            line_count = 0
            
            csv_writer.writeheader()
            for student in self.students:
                csv_writer.writerow(vars(student))
                
                line_count += 1
        pass
    
    def printAll(self):
        for student in self.students:
            student.printRow()

    def printRow(self, row):
        self.students[row].printRow()

    def deleteRow(self, row):
        self.students.pop(row)
    
    #creates a new student

    def addStudent(self,first, last, age, year, department, predictedGrade):
        self.students.append(Student(first, last, age, year, department, predictedGrade))

    #modifies a specific value for a student
    def editStudent(self, row, field, value):
        if field == 'name':
            names = value.split()
            setattr(self.students[row],'first',names[0])
            setattr(self.students[row],'last',names[1])
        else:
            setattr(self.students[row],field,value)

#this is the csv File (Should maybe be set in main, and later in a config file)
csvFile = csvReader('CSV readWrite\Students.csv')



class userInput:
    """
    This class listens for the user input and then calls the InputSwitch class, which is a custom switch case implementation
    """
    #   (This class proved to maybe not be worth adding ngl)
    def __init__(self):
        pass

    def listen(self):
        s = InputSwitch()
        return s.switch(input())

    pass

class InputSwitch:
    """
    Switch case implementation that gets the user input and chooses a code block based on the first word
    """

    #takes a passed in userinput string and returns the result of a function based on the first word. or a default return if not recognised
    def switch(self, userInput):
        self.userInput = userInput
        default = "Command not recognised. Type help to see available commands"
        return getattr(self, 'case_' + userInput.split()[0], lambda: print(default))()

    def case_help(self):
        print('Commands: help list view edit del add save')
        #todo add more detailed help instructions
        pass
    
    def case_list(self):
        csvFile.printAll()
        pass

    def case_view(self):
        #todo add limit to entries and view page 2 e.t.c option
        try:
            csvFile.printRow(int(self.userInput.split()[1])-1)
            
        except:
            print('type better')
        pass

    def case_edit(self):

        try:
            row = int(self.userInput.split()[1])-1
            field = self.userInput.split()[2]
            csvFile.editStudent(row,field, input('enter new value \n'))

        except:
            print('type better')
        pass  

    def case_del(self):
        try:
            csvFile.deleteRow(int(self.userInput.split()[1])-1)
        except:
            print('type better')
            #todo add delete by name referance
        pass
  

    def case_add(self):
        first = input('Enter First Name:')
        last = input('Enter Last Name:')
        age =input('Enter age:')
        year = input('Enter year:')
        department = input('Enter department:')
        predictedGrade = input('Enter predicted grade:')
        csvFile.addStudent(first, last, age, year, department, predictedGrade)
        pass

    def case_save(self):
        csvFile.save()
        pass
    def case_search(self):
        #todo add search by attribute
        pass

    def case_exit(self):
        return -1
 

class main ():
    if __name__ == "__main__":

        #load CSV file
        csvFile.load()

        

        print('Welcome to the studnts csv editor thingy mk1.')
        print('the file at ' + csvFile.filepath + ' has '+ str(len(csvFile.students)) + ' entries')
        exit = 0
        #input loop
        while exit != -1:
            exit = userInput().listen()


    pass
